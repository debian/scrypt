Source: scrypt
Section: utils
Priority: optional
Maintainer: Barak A. Pearlmutter <bap@debian.org>
Build-Depends: debhelper-compat (= 13),
		autoconf-archive,
		libssl-dev
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.tarsnap.com/scrypt.html
Vcs-Git: https://salsa.debian.org/debian/scrypt.git
Vcs-Browser: https://salsa.debian.org/debian/scrypt

Package: scrypt
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: File encryption utility using scrypt for key derivation
 A simple password-based encryption utility which demonstrates the
 scrypt key derivation function.  On modern hardware and with default
 parameters, the cost of cracking the password on a file encrypted by
 scrypt enc is approximately 100 billion times more than the cost of
 cracking the same password on a file encrypted by openssl enc; this
 means that a five-character password using scrypt is stronger than a
 ten-character password using openssl.

Package: libscrypt-kdf1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Shared library for encryption using scrypt for key derivation
 A simple password-based encryption utility which demonstrates the
 scrypt key derivation function.  On modern hardware and with default
 parameters, the cost of cracking the password on a file encrypted by
 scrypt enc is approximately 100 billion times more than the cost of
 cracking the same password on a file encrypted by openssl enc; this
 means that a five-character password using scrypt is stronger than a
 ten-character password using openssl.
 .
 This package contains the shared library.

Package: libscrypt-kdf-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, libscrypt-kdf1 (= ${binary:Version})
Description: Development library for encryption using scrypt for key derivation
 A simple password-based encryption utility which demonstrates the
 scrypt key derivation function.  On modern hardware and with default
 parameters, the cost of cracking the password on a file encrypted by
 scrypt enc is approximately 100 billion times more than the cost of
 cracking the same password on a file encrypted by openssl enc; this
 means that a five-character password using scrypt is stronger than a
 ten-character password using openssl.
 .
 This package contains the development files.
